describe("Login", () => {
  it("login - success", () => {
    cy.visit("https://learn.quipper.com/en/login");
    cy.get('[data-test=username]').type("aghni.anugrah");
    cy.get('[data-test=password]').type("Test12345678");
    cy.get('[data-test=login] > .RippleWrapper--pwNJU').click();
    cy.contains("Home").should("be.visible")
  });

  it("login - fail", () => {
    cy.visit("https://learn.quipper.com/en/login");
    cy.get('[data-test=username]').type("aghni.anugrah");
    cy.get('[data-test=login] > .RippleWrapper--pwNJU').click();
    cy.contains("Could not login").should("be.visible");
  });

  it("login - keep me logged in", () => {
    cy.visit("https://learn.quipper.com/en/login");
    cy.get('[data-test=username]').type("aghni.anugrah");
    cy.get('[data-test=password]').type("Test12345678");
    cy.get('[data-test=remember_me]').click();
    cy.get('[data-test=login] > .RippleWrapper--pwNJU').click();
    cy.contains("Home", {timeout: 5000}).should("be.visible");
    cy.visit("https://learn.quipper.com/");
    cy.contains("Home").should("be.visible")
  });

  it("login page - responsive", () => {
    cy.visit("https://learn.quipper.com/en/login");
    cy.viewport('macbook-13');
    cy.wait(200);
    cy.viewport('iphone-x');
    cy.wait(200);
    cy.get('.LoginTemplate__Title--74ogI')
  });

  it("sign up - fail", () => {
    cy.visit("https://subscribe.quipper.com/signup/id");
    cy.get('.control > .button').click();
    cy.contains("First name is required.").should("be.visible");
  });

  it("sign up - from log in page", () => {
    cy.visit("https://learn.quipper.com/en/login");
    cy.get('.Login__TextLink--3I201 > span').click();
    cy.contains("Create Account", {timeout: 5000}).should("be.visible");
  });

  it("sign up - from landing page", () => {
    cy.visit("https://www.quipper.com/id/");
    cy.get('a[class="lpv1-header__item ripple"]').click();
    cy.contains("Create Account", {timeout: 5000}).should("be.visible");
  });

  it("sign up - from video page", () => {
    // different nav bar
    cy.visit("https://www.quipper.com/id/video/");
    cy.get('a[class="nav-link-button ripple"]').click();
    cy.contains("Create Account", {timeout: 5000}).should("be.visible");
  });

  it("sign up - form validation id", () => {
    cy.visit("https://subscribe.quipper.com/signup/id");
    cy.url().should("include", "id");
    cy.get(':nth-child(1) > :nth-child(1) > .field > .control > .input').type("1111");
    cy.contains("First name cannot include numbers and symbols.").should("be.visible");
    cy.get(':nth-child(2) > .field > .control > .input').type("1111");
    cy.contains("Last name cannot include numbers and symbols.").should("be.visible");
    cy.get(':nth-child(4) > .column > .field > .control > .input').type("aaaa");
    cy.contains("Phone number should only contain numbers.").should("be.visible");
    cy.get(':nth-child(5) > .column > .field > .control > .input').type("a");
    cy.contains("Please enter email in the correct format.").should("be.visible");
    cy.get(':nth-child(6) > .column > .field > .control > .input').type("aaa");
    cy.contains("Username must be between 4-20 characters.").should("be.visible");
    cy.get(':nth-child(1) > .control > .PasswordInput > .Input').type("a");
    cy.contains("Password must be a combination of letters and numbers.").should("be.visible");
    cy.get(':nth-child(1) > .control > .PasswordInput > .Input').type("1");
    cy.contains("Password must be at least 6 characters.").should("be.visible");
    cy.get(':nth-child(4) > .control > .PasswordInput > .Input').type("b");
    cy.contains("The password and confirmation password do not match.").should("be.visible");
    cy.get(':nth-child(6) > .column > .field > .control > .input').clear().type("abcdefghijklmnopqrstuvwxyz")
      .should("have.value", "abcdefghijklmnopqrst");
    cy.get('.checkmark').click();
    cy.get(':nth-child(8) > .column > .field > .control > .input').should("be.visible");
    cy.get('.control > .button').click();
    cy.contains("Enter your School Membership Number.").should("be.visible");
  });

  it("sign up page - responsive", () => {
    cy.visit("https://subscribe.quipper.com/signup/id");
    cy.contains("Create Account", {timeout: 5000}).should("be.visible");
    cy.viewport('macbook-13');
    cy.wait(200);
    cy.viewport('iphone-x');
    cy.wait(200);
  });

  it("session - logged out when token is cleared", () => {
    cy.visit("https://learn.quipper.com/en/login");
    cy.get('[data-test=username]').type("aghni.anugrah");
    cy.get('[data-test=password]').type("Test12345678");
    cy.get('[data-test=login] > .RippleWrapper--pwNJU').click();
    cy.contains("Home", {timeout: 5000}).should("be.visible");
    cy.clearCookie('learn_auth_token');
    cy.get(':nth-child(1) > .NavList__NavLink--kyzGV > span').click();
    cy.contains("Log in").should("be.visible")
  });

  // it("sign up with facebook", () => {
  //   cy.visit("https://subscribe.quipper.com/signup/id");
  //   cy.get('.fb-button').click();
  //   cy.window().should('have.property', 'top');
  //   cy.contains("Facebook").should("be.visible");
  // });

  it("login - test", () => {
    cy.visit("https://learn.quipper.com/en/login");
    cy.get('[data-test=username]').type("test@hotmail.com");
    cy.get('[data-test=password]').type("test"); //ayy lmao
    cy.get('[data-test=login] > .RippleWrapper--pwNJU').click()
  });
});
