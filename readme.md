# Quipper - Product QA Internship Test
#### Author: Aghni Anugrah Raesa</h4>

## Requirements
1. [NodeJs](https://nodejs.org/en/)

## Run Guide
Run ```npm start``` on this project's directory.
Warning: It might take some time during the first run.

## Description
End-to-end tests written with [Cypress](https://www.cypress.io/)
